
var trails = new Array();
var names = new Array();
var vocabs = new Array();

function keeperLi(bmb, trl, val){
	brdcmb = "<li id='"+trl+"'>";
	brdcmb+= bmb;
	brdcmb+= "<a href='#' onclick='removeSelection("+val+", \""+trl+"\"); return false;' alt='Remove'> x </a>";
	brdcmb+= "</li>";
	return brdcmb;
}

function removeSelection(val, trl){
	trails[val] = false;
	$("li[@id='"+trl+"']").remove();
	vid = trl.substring(0, trl.indexOf(','));
	$("#edit-terms-"+val).attr("checked", false);
}

function setCheckboxes(trail, vocabulary){
	checkBoxes = $('.form-checkbox', vocabulary);
	$.each(checkBoxes, function(i, n){
		if(trails[n.value]){
			$(this).attr("checked", true);
		}
	});
	checkBoxes.click(function(){		
		if(trails[this.value]){
			trails[this.value] = false;
			$("li[@id='"+trail+','+this.value+"']").remove();
		} else {
			trails[this.value] = trail+','+this.value;
			var res = trailToBreadCrumb(this.parentNode.textContent);
			$('#keeper').append(keeperLi(res, trail+","+ this.value, this.value));
		}
	});
}

// Brings lists of terms via ajax.
function sth(href){
	var vid = href.split('get/')[1].split('/').shift();
	var voc = $('#strider-'+vid);
	var broughtTerms = function(data){		
		var result = Drupal.parseJson(data);		
		// Drupal returns form inside <form> tags. Remove them.
		var form = $(result[1]).children().eq(0);
		var	output = form.html();		
		voc.fadeIn('fast').html(output);		
		trail = $("input[@id='trails_"+result[0]+"']").val();
		$("../legend/a", voc).html(trailToBreadCrumb2(trail));
		setCheckboxes(trail, voc);
		$('a[@class="tmystrider"]').unbind().click(function(){
			sth(this.href);
			return false;
		});
	};
	
	$.ajax(
		{
			'type': 'GET',
			'url':	href,
			'beforeSend': function(XHR){
				$("div.strider", voc).html('<p><strong>Loading...</strong></p>');
				$("#ts_breadcrumb").html();
			},
			'success': broughtTerms
		}
	);
}

function arrayJoin(element, separator){
	var flag = true;
	var value = "";
	for(i=0; i<element.length; i++){
		if(element[i]){
			if(flag){
				flag = false;
			} else {
				value+= separator;
			}
			value+= element[i];
		}
	}
	return value;
}

function trailToBreadCrumb(termname){
	var brdcmb = $('#edit-current').val() + " | " + termname;
	return brdcmb;
}

function trailToBreadCrumb2(trail){
	tids = trail.split(',');
	href = '?q=taxonomy_strider/get_term_name/';
	brdcmb = new Array();
	$.each(tids, function(i, n){
		if(names[n]==undefined){			
			$.ajax({
				async: false,
				type: "POST",
				url: href+n,
				data: "",
				success: function(data){					
					names[n] = data;
				}
			});
		}
		if(i!=0){
			brdcmb[i] = names[n];
		} else {
			brdcmb[i] = vocabs[n];
		}		
	});
	brdcmb = brdcmb.join(' | ');
	return brdcmb;
}

if(Drupal.jsEnabled){
	$(document).ready(function(){
		$('a.tmystrider').click(function(){
			sth(this.href);
			return false;
		});
	
		// Store vocabulary names.
		var	vids = $("input[@class='locator']");		
		$.each(vids, function(i, n){
			$('#edit-taxonomy-'+n.value).parent().hide();
			vocabs[n.value] = n.title;
		});	
		
		// Set trails array.
		rawTrailsValue = $('#edit-taxpaths').val();
		if(rawTrailsValue!=null && rawTrailsValue!=""){
			rawTrails = rawTrailsValue.split('|');
			$.each(rawTrails, function(i, n){
				res = trailToBreadCrumb2(n);
				tid = n.substr(n.lastIndexOf(',')+1);
				trails[tid] = n;
				$('#keeper').append(keeperLi(res, n, tid));
			});
		}
		
		vids.each(function(i){
			if($.browser.msie){
				sth('?q=taxonomy_strider/get/checkbox/1/'+this.value, this.value);
			} else {
				setCheckboxes($("#trails_"+this.value).attr('value'), this.parentNode);
			}
		});		
	});	
}