if(Drupal.jsEnabled){
	$(document).ready(function(){
		$('#node-form').submit(function(){
			var vid;
			values = arrayJoin(trails, '|');
			$('#edit-taxpaths').val(values);			
			$.each(trails, function(i, n){
				if(n){
					ctids = n.split(',');
					$.each(ctids, function(i, n){
						if(i==0){
							vid = n;
							ob = $('#edit-taxonomy-'+vid);
						} else {							
							$('<option />').attr('value', n).html(n).attr('selected', true).appendTo(ob);
						}
					});
				}
			});
			$('.strider').remove(); // Custom checkboxes bring conflict.			
		});			
	});
}